/**
 * Created by phangty on 19/10/16.
 */
(function(){
    angular
        .module("SequelizeApp")
        .controller("EmployeesCtrl", EmployeesCtrl);

    function EmployeesCtrl(employeesService){
        var vm = this;
        vm.employees = {
            firstName: "",
            lastName: "",
            birthDate: "",
            hireDate: "",
            gender: "",
            empNo: ""
        };

        vm.searchName = "";
        vm.employeeNo = "";
        vm.searchResult = [];

        vm.createEmployee = createEmployee;
        vm.updateEmployee = updateEmployee;
        vm.getEmployees = getEmployees;
        vm.getEmployee = getEmployee;
        vm.deleteEmployee = deleteEmployee;
        vm.navigateTab = navigateTab;
        vm.navigatePage = ""

        function navigateTab(page){
            vm.navigatePage = page;
        }

        console.log(vm.employees);

        function createEmployee(){
            console.log("create Employee");
            employeesService.saveEmployee(vm.employees)
                .then(function(result){
                    console.info("Successful sent");
            }).catch(function (err) {
                console.info("Some Error Occured",err);
            });
        } // END create

        function updateEmployee(){
            console.log("Update Employee");
            vm.employees.empNo = vm.employeeNo;
            employeesService.updateEmployee(vm.employees)
                .then(function(results){
                    console.info("Successful sent");
                    console.log(results.data);
                    vm.searchResult = results.data;
                    console.log(vm.searchResult);
                }).catch(function (err) {
                console.info("Some Error Occured",err);
            });
        } // END create

        function getEmployees(){
            console.log("Get Employees");
            employeesService.getEmployees(0, 20, vm.searchName)
                .then(function(results){
                    console.info("Successful sent");
                    console.log(results.data);
                    vm.searchResult = results.data;
                    console.log(vm.searchResult);
                }).catch(function (err) {
                console.info("Some Error Occured",err);
            });
        } // get Employees

        function getEmployee(){
            console.log("Get Employee");
            console.log(vm.employeeNo);
            employeesService.getEmployee(vm.employeeNo)
                .then(function(results){
                    console.info("Successful sent");
                    console.log(results.data);
                    vm.employees.firstName = results.data.first_name;
                    vm.employees.lastName = results.data.last_name;
                    vm.employees.gender = results.data.gender;
                    vm.employees.hireDate = new Date(results.data.hire_date);
                    vm.employees.birthDate = new Date(results.data.birth_date);
                }).catch(function (err) {
                console.info("Some Error Occured",err);
            });
        } // get EMployee


        function deleteEmployee(empNo){
            console.log(empNo);
            employeesService.deleteEmployee(empNo)
                .then(function(results){
                    console.info("Successful sent");
                    console.log(results);
                    getEmployees();
                }).catch(function (err) {
                console.info("Some Error Occured",err);
            });
        } // END create


    } // End CreateCtrl

    EmployeesCtrl.$inject = ['employeesService'];
})();
