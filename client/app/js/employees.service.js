/**
 * Created by phangty on 19/10/16.
 */
(function () {
    angular.module("SequelizeApp")
        .service("employeesService", employeesService);

    employeesService.$inject = ["$http", "$q"];

    function employeesService($http, $q) {
        var service = this;

        service.getEmployee = function (empNo){
            var defer = $q.defer();
            console.log(empNo);
            console.log("xxxxx");

            $http.get("/api/employee/" + empNo)
                .then(function(result){
                    defer.resolve(result);
                }).catch(function(error){
                defer.reject(error);
            });
            return defer.promise;
        };

        service.getEmployees = function (offset, recPerpage, name){
            var defer = $q.defer();
            console.log(name);
            $http.get("/api/employees/" + offset + "/" + recPerpage + "/"+ name)
                .then(function(results){
                    defer.resolve(results);
                }).catch(function(error){
                defer.reject(error);
            });
            return defer.promise;
        };


        service.saveEmployee = function (data){
            console.log(data);
            var defer = $q.defer();
            $http.post("/api/employees/", {
                    params: data
                 })
                .then(function(result){
                    defer.resolve(result);
                }).catch(function(error){
                defer.reject(error);
            });
            return defer.promise;
        };

        service.updateEmployee = function (data){
            var defer = $q.defer();
            $http.put("/api/employees/", {params: data} )
                .then(function(results){
                    defer.resolve(results);
                }).catch(function(error){
                defer.reject(error);
            });
            return defer.promise;
        };

        service.deleteEmployee = function (data){
            var defer = $q.defer();
            console.log(data);
            $http.delete("/api/employees/", {params: {empNo: data}})
                .then(function(result){
                    defer.resolve(result);
                }).catch(function(error){
                defer.reject(error);
            });
            return defer.promise;
        };

    }
})();