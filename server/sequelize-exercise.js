/**
 * Created by phangty on 24/10/16.
 */
var Sequelize = require('sequelize');
var randomstring = require("randomstring");

// database name  , username and password
// get connection
var conn = new Sequelize('employees', 'root', 'password@123',
{
    host: "localhost",
    dialect: "mysql",
    pool: {
        max: 4,
        min: 0,
        idle: 10000
    }
});

// test the connection
conn.authenticate().then(function (err){
    console.log('Connection has been established successfully.');
}).catch(function (err){
    console.log('Unable to connect to the database:', err);
});



const publisher = conn.define("publisher",{
    salutation: {
        type: Sequelize.STRING,
        defaultValue: "Mr."
    },
    name: {
        type: Sequelize.STRING,
        allownull: true
    },
    id_no: {
        type: Sequelize.STRING,
        unique: true
    },
    age: {
        type: Sequelize.INTEGER,
        validate: {
            ageAbove18: function(ageValue){
                if(ageValue < 18){
                    throw new Error("Only 18 and above!");
                }
            }
        }
    },
    email: {
        type: Sequelize.STRING,
        unique: true,
        validate: {
            isEmail: {
                args: true,
                msg: "Invalid Email Adress"
            }

        }
    }

},{
    timestamps: false,
    freezeTableName: true,
    hooks: {
        beforeValidate: function(publisher) {
            console.log("beforeValidate");
        },
        afterValidate: function(publisher) {
            console.log("afterValidate");
        },
        beforeCreate: function(publisher){
            console.log("beforeCreate");
        },
        afterCreate: function(publisher){
            console.log("afterCreate");
        }
    }
});

// define objects
const blogs = conn.define("blog", {
    title: {
        type: Sequelize.STRING,
        validate: {
            len: [10, 200]
        }
    },
    content: Sequelize.TEXT,
    publish_date: Sequelize.DATE,
    publisherId: {
    type: Sequelize.INTEGER,
        references: {
            model: "publisher",
            key: "id"
        }
    }
}, {
    timestamps: false
});

const layout = conn.define("layout", {
   layout_id: {
       type: Sequelize.STRING,
       primaryKey: true,
   },
   layout_colour: {
       type: Sequelize.STRING,
       validate: {
           len: [3, 150]
       }
   }
},{
    timestamps: true

});


//conn.sync();

/*
blogs.findAll().then(function(blogs) {
    console.log("Empty collection !");
    console.log(blogs);
});
*/

conn.sync({
    logging: console.log,
    force: true
}).then(function(){

    /*
     publisher.create({
        id_no: randomstring.generate(7),
        age:29
     }).then(function(insertedPublisher){
         console.log(insertedPublisher);

         blogs.create({
             title: "Test Kenneth2",
             content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. " +
             "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, " +
             "when an unknown printer took a galley of type and scrambled it to make a type " +
             "specimen book. It has survived not only five centuries, but also the leap " +
             "into electronic typesetting, remaining essentially unchanged. It was " +
             "popularised in the 1960s with the release of Letraset sheets containing " +
             "Lorem Ipsum passages, and more recently with desktop publishing software " +
             "like Aldus PageMaker including versions of Lorem Ipsum.",
             publish_date: new Date(),
             publisherId: insertedPublisher.id
         });
     });*/


    /*
    publisher.hasMany(blogs);
    publisher.findAll({ include: [ blogs ] }).then(function(p) {
        console.log(p);
    });*/

    /*
    blogs.create({
       title: "Test Kenneth2",
       content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. " +
       "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, " +
       "when an unknown printer took a galley of type and scrambled it to make a type " +
       "specimen book. It has survived not only five centuries, but also the leap " +
       "into electronic typesetting, remaining essentially unchanged. It was " +
       "popularised in the 1960s with the release of Letraset sheets containing " +
       "Lorem Ipsum passages, and more recently with desktop publishing software " +
       "like Aldus PageMaker including versions of Lorem Ipsum.",
       publish_date: new Date()
    });*/


    /*
    blogs.count({ where: ["id > ?", 0] }).then(function(c) {
        console.log(c);
    });*/


    /*
    blogs.findById(2).then(function(blog){
        console.log(blog.id);
        console.log(blog.title);
        console.log(blog.content);
        console.log(blog.publish_date);
    });*/



    layout.create({
        layout_id: randomstring.generate(7),
        layout_colour: "White"
    });

    /*
    layout.create({
        layout_id: randomstring.generate(7),
        layout_colour: "Re"
    });*/


    /*
    publisher.create({
        id_no: "S7780374X",
        age:29,
        email: "kk.com"
    });*/

    /*
    publisher.create({
        id_no: "S7780374Z",
        age:17,
        email: "kks@f.com"
    });*/


/*
     publisher.create({
     id_no: "S7780374Z",
     age:20,
     email: "kks@f.com"
     });
*/


/*
    publisher.bulkCreate([
        {
            id_no: "S7780374Y",
            age:20,
            email: "kks1@f.com"
        },
        {
            id_no: "S7780364Y",
            age:20,
            email: "kks2@f.com"
        },
        {
            id_no: "S7780354Y",
            age:20,
            email: "kks3@f.com"
        },
        {
            id_no: "S7780344Y",
            age:20,
            email: "kks4@f.com"
        }
    ]);*/


    conn.transaction(function (t) {
        return publisher.create({
            id_no: "S7781344Y",
            age:23,
            email: "kks6@f.com"
        }, {transaction: t}).then(function (publisher) {
            return blogs.create({
                title: "1234567890",
                content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. " +
                "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, " +
                "when an unknown printer took a galley of type and scrambled it to make a type " +
                "specimen book. It has survived not only five centuries, but also the leap " +
                "into electronic typesetting, remaining essentially unchanged. It was " +
                "popularised in the 1960s with the release of Letraset sheets containing " +
                "Lorem Ipsum passages, and more recently with desktop publishing software " +
                "like Aldus PageMaker including versions of Lorem Ipsum.",
                publish_date: new Date(),
                publisherId: publisher.id
            }, {transaction: t});
        }).catch(function (err) {
            throw new Error(err);
        });
    });

});