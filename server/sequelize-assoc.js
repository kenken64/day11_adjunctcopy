/**
 * Created by phangty on 24/10/16.
 */
"use strict";
var c ={
    database:"employees",
    username:"root",
    password:"password@123",
    dialect: "mysql",
    port:"3306",
    force:false

}
var Promise = require("bluebird");
var Sequelize = require("sequelize");
var sequelize = new Sequelize( c.database,c.username,
    c.password,{dialect :'mysql', port:'3306'});


var Task = sequelize.define( 'Task',
    {
        id:{ type: Sequelize.INTEGER,primaryKey:true, autoIncrement:true},
        name:{type:Sequelize.TEXT,allowNull:false},
        begin_time:{type:Sequelize.DATE,allowNull:false,defaultValue:Sequelize.NOW},
        end_time:{type:Sequelize.DATE,allowNull:true},
        user_id: {
            type: Sequelize.INTEGER,
            references: {
                model: "User",
                key: "id"
            }
        }
    },
    {
        tableName:"task",
        underscored:true,
        timestamps:false

    }

);

var Address = sequelize.define( 'Address',
    {
        id:{ type: Sequelize.INTEGER,primaryKey:true, autoIncrement:true},
        addr_1:{ type:Sequelize.TEXT, allowNull:false},
        addr_2:{type:Sequelize.TEXT, allowNull:true},
        zip_code:{type:Sequelize.TEXT,allowNull:false},
        user_id: {
            type: Sequelize.INTEGER,
            references: {
                model: "User",
                key: "id"
            }
        }
    },
    {
        tableName:"address",
        underscored:true,
        timestamps:false

    }
);

var User =sequelize.define('User',{
        id:{ type: Sequelize.INTEGER,primaryKey:true, autoIncrement:true},
        name:{type:Sequelize.TEXT,allowNull:false},
        nick_name:{type:Sequelize.TEXT,allowNull:true},
        date_of_joining:{type:Sequelize.DATE,allowNull:false,defaultValue:Sequelize.NOW}

    },
    {
        tableName:"user",
        underscored:true,
        timestamps:false

    }
);


User.hasMany(Task);
User.hasOne(Address);


sequelize.sync( {force:false}, {logging:console.log})
    .then( function( args ){
        User.create({
            name: "Kenneth",
            nick_name: "kenken64",
            date_of_joining: new Date()
        }).then(function(insertedUser){
            Task.create({
                name: "Task 1",
                begin_time: new Date(),
                end_time: new Date(),
                user_id: insertedUser.id
            });

            Address.create({
                addr_1: "Blk 333 Ang Mo Kio Ave 1",
                addr_2: "#11-1909",
                zip_code: "56033",
                user_id: insertedUser.id
            });
        });

        User.findAll({
            include: [Task, Address]
        })
            .then(function (results) {
                console.log(results);
                var resultValues = JSON.stringify(results);
                var resultValues2 = JSON.parse(resultValues);
                console.log(resultValues2);
            })
            .catch(function (error) {
                console.log(error);
            });
    });