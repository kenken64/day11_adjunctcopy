/**
 * Created by phangty on 19/10/16.
 */
'use strict';

var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');
var basename  = path.basename(module.filename);
var env       = process.env.NODE_ENV || 'development';
var config    = require(__dirname + '/../config/config.json')[env];
var db        = {};


console.log("Development : " + config.use_env_variable);
console.log("Database name : " + config.database);
console.log("Username : " + config.username);
console.log("Password : " + config.password);

if (config.use_env_variable) {
    console.log("using the array");
    var sequelize = new Sequelize(process.env[config.use_env_variable]);
} else {
    console.log("using the non-array");
    var sequelize = new Sequelize(config.database,
        config.username, config.password, config);
}

// read current directory whenever see a model js load into the sequelize ORM
fs
    .readdirSync(__dirname)
    .filter(function(file) {
        return (file.indexOf('.') !== 0) && (file !== basename);
    })
    .forEach(function(file) {
        console.log("Model " + file);
        if (file.slice(-3) !== '.js') return;
        var model = sequelize['import'](path.join(__dirname, file));
        console.log("Model " + model);
        db[model.name] = model;
    });

Object.keys(db).forEach(function(modelName) {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;