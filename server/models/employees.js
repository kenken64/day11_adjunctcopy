/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  var Employees=  sequelize.define('employees', {
    emp_no: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    birth_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    first_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    last_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    gender: {
      type: DataTypes.ENUM('M','F'),
      allowNull: false
    },
    hire_date: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    classMethod: {
      associate: function(models) {
        Employees.hasMany(models.salaries, {
          onDelete: 'cascade'
        });

        Employees.hasMany(models.dept_emp, {
          onDelete: 'cascade'
        });
      }
    },
    tableName: 'employees',
    timestamps: false
  });
  return Employees;
};
