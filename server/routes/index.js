/**
 * Created by phangty on 21/10/16.
 */
var employees = require('./employees.js')
var deptEmployees = require('./dept_employee.js')
var departments = require('./departments.js')


module.exports.set = function(app) {

    // initialize the routes for the web app.
    employees.set(app);
    deptEmployees.set(app);
    departments.set(app);

};

