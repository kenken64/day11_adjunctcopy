/**
 * Created by phangty on 24/10/16.
 */
var models  = require('../models/index');

module.exports.set = function(app){
    const DEPARTMENTS_API_URL = "/api/departments/";

    app.get(DEPARTMENTS_API_URL, function (req, res) {
        models.departments.findAndCountAll({ limit: 2000 }).then(function (result) {
            console.log(result.rows);
            if (result && result.rows) {
                res.json(result.rows);
            } else {
                res.status(400).send(JSON.stringify("Record Not Found"));
            }
        });
    });
}