/**
 * Created by phangty on 21/10/16.
 */

var models = require('../models/index');

module.exports.set = function(app){

    const EMPLOYEE_API_URL = "/api/employees/";

    app.get("/api/employee/:emp_no", function (req, res) {
        console.info(">>> emp_no = %s", req.params.emp_no);
        models.employees.find({where: {emp_no: req.params.emp_no}}).then(function (result) {
            if (result && result.dataValues) {
                res.json(result.dataValues);
            } else {
                res.status(400).send(JSON.stringify("Record Not Found"));
            }
        });
    });


    app.get(EMPLOYEE_API_URL, function (req, res) {
        models.employees.findAndCountAll({ limit: 100 }).then(function (result) {
            if (result && result.rows) {
                res.json(result.rows);
            } else {
                res.status(400).send(JSON.stringify("Record Not Found"));
            }
        });
    });

    app.get(EMPLOYEE_API_URL + ":offset/:perpage/:name", function (req, res) {
        console.info(">>> offset/perpage ", req.params);

        models.employees.findAndCountAll
            ({where: {$or: [{first_name: req.params.name}, {last_name: req.params.name}]}, offset: parseInt(req.params.offset)
                , limit: parseInt(req.params.perpage)})
                .then(function (results) {
                    //console.log(results.rows);
                    res.status(200).json(results.rows);
        });

    });

    app.post(EMPLOYEE_API_URL, function (req, res) {
        console.log(req.body.params);
        console.log(req.body);
        models.employees.create(
            {
                first_name: req.body.params.firstName,
                last_name: req.body.params.lastName,
                birth_date: new Date(req.body.params.birthDate),
                gender: req.body.params.gender,
                hire_date: new Date(req.body.params.hireDate)
            })
            .then(function (results) {
                console.log(results);
                res.status(200).json(results);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            });
    });


    app.put(EMPLOYEE_API_URL, function (req, res) {
        console.log(req.body.params);
        console.log(req.body);
        models.employees.find({ where: { emp_no: req.body.params.empNo } })
            .then(function (employee) {
                // Check if record exists in db
                if (employee) {
                    employee.updateAttributes({
                        first_name: req.body.params.firstName,
                        last_name: req.body.params.lastName,
                        birth_date: new Date(req.body.params.birthDate),
                        gender: req.body.params.gender,
                    }).then(function () {
                        console.log("Update successful.");
                        res.status(200).json(employee);
                    })
                }
            });
    });


    /**
     * Delete employee record by Emp no.
     */
    app.delete(EMPLOYEE_API_URL, function (req, res) {
        console.log("Delete");
        console.log(req.query.empNo);

        models.employees.destroy({
            where: {
                emp_no: req.query.empNo
            }
        }).then(function(result){
            console.log(result);
            res.status(200).json(result);
        })

    });


}